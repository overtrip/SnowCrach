/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jealonso <jealonso@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 15:16:24 by jealonso          #+#    #+#             */
/*   Updated: 2017/08/22 15:26:32 by jealonso         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int	ft_strlen(char *str)
{
	int	ret;

	ret = 0;
	while (str[ret])
		++ret;
	return (ret);
}

void	ft_putstr(char *str)
{
	write(1, str, ft_strlen(str));
}

void	ft_putendl(char *str)
{
	ft_putstr(str);
	write(1, "\n", 1);
}

int main(int ac, char **av)
{
	int		size;
	int		cmp;
	char	*str;

	if (ac != 2)
	{
		ft_putendl("pas le bon nombre d'arguments.");
		return (2);
	}
	size = ft_strlen(av[1]);
	if(!(str = (char *)malloc(sizeof(char) * size + 1)))
	{
		ft_putendl("Pas de malloc.");
		return (2);
	}
	cmp = -1;
	while (++cmp < size)
		str[cmp] = av[1][cmp] - (char)cmp;
	str[cmp] = '\0';
	ft_putendl(str);
	return (0);
}
